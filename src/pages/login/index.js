import React , {Fragment, Component} from "react";
import { Button, Form, FormGroup, Label, Input, FormText,
    Container, Row, Col} from 'reactstrap';
import {connect} from "react-redux";

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: false,
            spoilerActive: false
        }
    }

    render() {
        let {errors, spoilerActive} = this.state;
        return (
            <Fragment>
                <Container>
                    <Row className="auth">
                        <Col lg={{size: 6, offset: 3}} md={{size: 8, offset: 2}} sm={{size: 12, offset: 0}} xs={{size: 12, offset: 0}}>
                            <h2 className="auth__title">
                                Авторизация
                            </h2>
                            <Form className="auth__form form-auth" onSubmit={(e) => this.login(e)}>
                                <FormGroup className="form-auth__group">
                                    <Label for="login">Логин</Label>
                                    <Input type="text" name="login" id="login" placeholder="Логин" required/>
                                </FormGroup>
                                <FormGroup className="form-auth__group">
                                    <Label for="password">Пароль</Label>
                                    <Input type="password" name="password" id="password" placeholder="Пароль" required/>
                                </FormGroup>
                                <FormText className={`inputError__text ${errors ? "active" : ""}`}>Неверный логин и/или пароль</FormText>
                                <Button className="form-auth__btn">Войти</Button>
                            </Form>
                            <div className="auth__spoiler spoiler">
                                <div className="spoiler__title" onClick={this.toggleSpoiler}>Спойлер</div>
                                <ul className={`spoiler__list ${spoilerActive ? "active" : ""}`}>
                                    <li>Логин: login</li>
                                    <li>Пароль: 123456</li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        )
    }

    login = (e) => {
        e.preventDefault();
        let formData = new FormData(e.target);
        if (formData.get("login") === "login" && formData.get("password") === "123456") {
            this.handleErrors(false);
            this.props.dispatch({
                type: "LOGIN",
                data: {
                    name: "isAuth",
                    value: true,
                    options: {
                        "max-age": 3600
                    }
                }
            })
        } else {
            this.handleErrors(true);
        }
    }

    handleErrors = (visible) => {
        this.setState({
            errors: visible
        })
    }

    toggleSpoiler = () => {
        this.setState({
            spoilerActive: !this.state.spoilerActive
        })
    }


}


let mapStateFromProps = (store) => {
    return {store: store.auth}
}
export default connect(mapStateFromProps)(Login)