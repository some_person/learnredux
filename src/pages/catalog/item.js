import React , {Fragment, Component} from "react";
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button,
    Row, Col, Container,
    Modal, ModalHeader, ModalBody
} from 'reactstrap';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import Filter from "./../../components/filter/index";
import Add from "../../components/handlerBasket/add";

export default class CatalogItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {product} = this.props;
        return (
            <Fragment>
                <Card className="catalog__item item-catalog">
                    <CardBody>
                        <CardTitle className="item-catalog__title">
                            <Link to={`product/${product.id}`}>{product.title}</Link>
                        </CardTitle>
                        <Link className="item-catalog__img" to={`product/${product.id}`}>
                            <img src={product.src} alt={product.title}/>
                        </Link>
                        <CardSubtitle className="item-catalog__price">
                            <p>Цена: <span>{Math.floor(product.price * 100) / 100}</span> BYN</p>
                        </CardSubtitle>
                        <Add productId={product.id} toggleModalAddedToBasket={this.toggleModal}/>
                    </CardBody>
                </Card>
            </Fragment>
        )
    }

    toggleModal = () => {
        this.props.toggleModalAddedToBasket();
    }
}
