import React , {Fragment, Component} from "react";
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button,
    Row, Col, Container,
    Modal, ModalHeader, ModalBody
} from 'reactstrap';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import Filter from "./../../components/filter/index";
import Add from "./../../components/handlerBasket/add";
import Item from "./item";

class Catalog extends Component {
    constructor(props) {
        super(props);

        this.filterHandlers = {
            title: this.filterByTitle,
            price: this.filterByPrice,
        }

        // this.filtersFromStore = [];

        this.state = {
            products: [],
            filtersFromStore: {},
            showModalAddedToBasket: false,
        }
    }

    render() {
        let {filters} = this.props.store;
        let products = this.getVisibleProducts(filters.activeFilters);
        let {showModalAddedToBasket} = this.state;
        return (
            <Fragment>
                    <Container className="catalog">
                        <Filter/>
                        {products.length ?
                            <Row>
                                {products.map(product => {
                                    return (
                                        <Col lg={3} md={4} sm={6} xs={12} key={product.id}>
                                            <Item toggleModalAddedToBasket={this.toggleModalAddedToBasket} product={product}/>
                                        </Col>)
                                })}


                                <Modal isOpen={showModalAddedToBasket} toggle={this.toggleModalAddedToBasket}>
                                    <ModalHeader toggle={this.toggleModalAddedToBasket}>Товар успешно добавлен в корзину</ModalHeader>
                                    <ModalBody>
                                        <Link to={"basket/"}>Перейти в корзину</Link>
                                    </ModalBody>
                                </Modal>
                            </Row> :
                            <Row>
                                <Col>Ничего не найдено</Col>
                            </Row>
                        }
                    </Container>
            </Fragment>
        )
    }

    componentDidMount() {
        this.setState({
            products: [...this.props.store.products]
        })
    }

    getVisibleProducts = (filters) => {
        console.log(filters, 99)
        let visibleProducts = [...this.props.store.products];
        if (this.filtersEmpty(filters)) {
            return visibleProducts;
        } else {
            let {filterHandlers} = this;
            for (let key in filters) {
                if (filters.hasOwnProperty(key) && filters[key].length) {
                    visibleProducts = filterHandlers[key](visibleProducts, filters[key]);
                }
            }

        }
        return visibleProducts;
    }

    filtersEmpty = (filters) => {
        for (let key in filters) {
            if (filters.hasOwnProperty(key)){
                if (filters[key].length) {
                    return false;
                }
            }
        }
        return true;
    }

    filterByTitle = (products, titles) => {
        let _products = [...products];
        let _titles = [...titles];
        let regexpInner = _titles.join("|");
        let regexp = new RegExp(`\^${regexpInner}`, "i");
        return _products.filter(product => {
            let matches = regexp.test(product.title);
            if (matches) return product;
        });

    }

    filterByPrice = (products, range) => {
        let _products = [...products];
        let _range = [...range];
        return _products.filter(product => {
            if (product.price >= _range[0] && product.price <= _range[1]) {
                return product
            }
        })
    }

    toggleModalAddedToBasket = () => {
        this.setState({
            showModalAddedToBasket: !this.state.showModalAddedToBasket
        })
    }


}

let mapStateFromProps = (store) => {
    return {store: store}
}
export default connect(mapStateFromProps)(Catalog)