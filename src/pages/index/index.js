import React , {Fragment, Component} from "react";
import {Container, Row, Col} from "reactstrap";
import MainSlider from "./../../components/sliders/mainSlider"
export default class MainPage extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <Fragment>
                <Container>
                    <Row className="intro">
                        <Col lg={4} md={12} className="intro__text">
                            <h2 className="intro__title">
                                КОЖАНЫЕ ИЗДЕЛИЯ «LOREM»
                            </h2>
                            <div className="intro__info info-intro">
                                <p className="info-intro__text">
                                    Доставка КУРЬЕРОМ по Беларуси БЕСПЛАТНО (при заказе от 50р)
                                </p>

                                <p className="info-intro__text">
                                    Доставка может быть организована:
                                </p>
                                <p className="info-intro__text">
                                    почтовым курьером - срок доставки 1 день или до почтового отделения - срок доставки 2-3 дня
                                </p>
                            </div>
                        </Col>
                        <Col lg={{size: 7, offset: 1}} md={{size: 12, offset: 0}} className="intro__slider slider-intro">
                            <h3 className="slider-intro__title">
                                ТОП Товары
                            </h3>
                            <MainSlider/>
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        )
    }
}

