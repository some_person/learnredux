import React , {Fragment, Component} from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import { Row, Col, Container,
    Modal, ModalHeader, ModalBody
} from 'reactstrap';
import ProductGallerySlider from "./../../components/sliders/productGallerySlider";
import Add from "../../components/handlerBasket/add";

class Product extends Component {
    constructor(props) {
        super(props);

        this.state = {
            product: {},
            slider: null,
            sliderMini: null,
            showModalAddedToBasket: false
        }
    }

    render() {
        let {products} = this.props;
        let {showModalAddedToBasket} = this.state;
        let product = products.find(product => product.id == this.props.match.params.id);
        let gallery = product.gallery.length ? [product.src, ...product.gallery] : [product.src];
        return (
            <Fragment>
                <Container>
                    <Row className="product-card">
                        <Col lg={12} className="product-card__left">
                            <h1 className="product-card__title">{product.title} <span className="product-card__article"> Артикул: {product.id}-000-0{product.id}0</span></h1>
                        </Col>

                        <Col lg={{size: 5, offset: 0}} md={{size: 6, offset: 0}} sm={{size:12, offset: 0}} xs={{size: 12, offset: 0}} className="product-card__left">
                            <ProductGallerySlider gallery={gallery}/>
                        </Col>
                        <Col  className="product-card__info card-info" lg={{size:6, offset: 1}} md={{size:6, offset: 0}} sm={{size:12, offset: 0}}>
                            <h2 className="card-info__title">Информация о товаре</h2>
                            <h3 className="card-info__subtitle">Описание</h3>
                            <p  className="card-info__desc">{product.full_text}</p>
                            <p className="card-info__price">Цена: <span>{product.price}</span> BYN</p>
                            <Add productId={product.id} toggleModalAddedToBasket={this.toggleModalAddedToBasket}/>
                        </Col>
                    </Row>

                    <Modal isOpen={showModalAddedToBasket} toggle={this.toggleModalAddedToBasket}>
                        <ModalHeader toggle={this.toggleModalAddedToBasket}>Товар успешно добавлен в корзину</ModalHeader>
                        <ModalBody>
                            <Link to={"/basket"}>Перейти в корзину</Link>
                        </ModalBody>
                    </Modal>

                </Container>
            </Fragment>
        )
    }

    toggleModalAddedToBasket = () => {
        this.setState({
            showModalAddedToBasket: !this.state.showModalAddedToBasket
        })
    }


}


let mapStateFromProps = (store) => {
    return {products: store.products}
}
export default connect(mapStateFromProps)(Product)