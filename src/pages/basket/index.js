import React , {Fragment, Component} from "react";
import {connect} from "react-redux";
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button,
    Row, Col, Container,
    Form, FormGroup, Label, Input, FormText,
    Modal, ModalHeader, ModalFooter
} from 'reactstrap';
import {Link} from "react-router-dom";
import BasketItem from "./item";

class Basket extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: {
                amountError: "Некорректное значение",
                IDs: []
            },
            showModalClearBasket: false
        }
    }

    render() {
        console.log('render')
        let {products, basket} = this.props.store;
        let {showModalClearBasket} = this.state;
        // Filter products in default store,
        let productsInBasket = products.filter(product => {
            if (basket.some(item => item.id === product.id)) {
                return product
            }
            // then adding the amount to each of them from store.basket
        }).map(product => {
            return {
                ...product,
                amount: basket.find(item => item.id === product.id).amount
            }
        });
        let generalAmount = productsInBasket.reduce((previousVal, currentVal) => {
            return previousVal + (+currentVal.amount)
        }, 0);
        let generalPrice = productsInBasket.reduce((previousVal, currentVal) => {
            return previousVal + currentVal.price * (+currentVal.amount)
        }, 0);
        // console.log(productsInBasket);
        return (

        <Fragment>
            <div className="basket">
                <Container>
                    <div className="basket__body">
                        <Row>
                            <Col lg={12}>
                                <h3 className="basket__title">
                                    Корзина
                                </h3>
                            </Col>
                        </Row>
                        <Row className="basket__row basket__row_top">
                            <Col lg={12}>
                                <h3 className="block__title">
                                    Товары
                                </h3>
                            </Col>
                        </Row>
                        <Row className="basket__row">
                            <Col lg={6} md={6} sm={6} xs={12} className="basket__amount">
                                <p>
                                    У Вас в корзине {generalAmount} товар(-а, -ов)
                                </p>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className="basket__remove-all">
                                <a href="#!" onClick={this.toggleModalClearBasket}>
                                    Удалить все
                                </a>
                            </Col>
                        </Row>
                        {productsInBasket.length ?
                            productsInBasket.map(product => {
                                return <BasketItem key={product.id} product={product} remove={this.removeProduct} update={this.updateProduct}/>
                            }) :
                        <Row className="basket__row">
                            <Col lg={12}>
                                <Link to="catalog/" className="goTo">Перейти в каталог</Link>
                            </Col>
                        </Row>
                        }
                    </div>
                    <Row>
                        <Col lg={{size: 6, offset: 6}} className="basket__price">
                            <p>Общая цена: <span className="basket__price_c">{Math.floor(generalPrice * 100) / 100}</span> BYN</p>
                        </Col>
                    </Row>
                </Container>
                <Modal isOpen={showModalClearBasket} toggle={this.toggleModalClearBasket}>
                    <ModalHeader toggle={this.toggleModalClearBasket}>Очистить корзину?</ModalHeader>
                    <ModalFooter>
                        <Button color="danger" onClick={this.clearBasket}>Да</Button>{' '}
                        <Button color="secondary" onClick={this.toggleModalClearBasket}>Отмена</Button>
                    </ModalFooter>
                </Modal>

            </div>
        </Fragment>
        )
    }

    updateProduct = (e, id, amount) => {
        this.props.dispatch({
            type: "UPDATE_BASKET_PRODUCT_AMOUNT",
            data: {
                amount: +amount,
                id: id
            }
        })
    }

    removeProduct = (e, productId) => {
        this.setState({
            showModalRemoveProduct: !this.state.showModalRemoveProduct
        });
        this.props.dispatch({
            type: "REMOVE_PRODUCT",
            data: {
                id: productId
            }
        })
    }

    toggleModalClearBasket = () => {
        this.setState({
            showModalClearBasket: !this.state.showModalClearBasket
        })
    }

    clearBasket = () => {
        this.setState({
            showModalClearBasket: !this.state.showModalClearBasket
        });
        this.props.dispatch({
            type: "CLEAR_BASKET"
        })
    }
}

let mapStateFromProps = (store) => {
    return {store: store}
}
export default connect(mapStateFromProps)(Basket)
