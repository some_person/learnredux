import React , {Fragment, Component} from "react";
import { Button,
    Row, Col, Container,
    Form, FormGroup, Label, Input, FormText,
    Modal, ModalHeader, ModalFooter
} from 'reactstrap';
import {Link} from "react-router-dom";

export default class BasketItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            amountError: false,
            showModalRemoveProduct: false
        }
    }

    render() {
        let {product} = this.props;
        let {amountError, showModalRemoveProduct} = this.state;
        // console.log(productsInBasket);
        return (
            <Fragment>
                <Row className="basket__row item" key={product.id}>
                    <Col className="item__image" lg={2} md={2} sm={4} xs={12}>
                        <img src={product.src} alt="image"/>
                    </Col>
                    <Col className="item__name" lg={{size:3, offset: 1}} md={{size:3, offset: 1}} sm={{size:4, offset: 0}}>
                        <Link to={`product/${product.id}`}>{product.title} Артикул: {product.id}-000-0{product.id}0</Link>
                    </Col>
                    <Col className="item__amount" lg={6} md={6} sm={{size:4, offset: 0}} xs={12}>
                        <Form onSubmit={(e) => this.updateProduct(e, product.id)}>
                            <Row form>
                                <Col md={{size: 4, offset: 2}} xs={{size: 4, offset: 0}}>
                                    <FormGroup>
                                        <div className="input__wrap">
                                            <Input type="number" name="item__amount" id="item__amount" defaultValue={product.amount}/>
                                            {amountError ? <p className="inputError__text">Некорректное занчение</p> : ""}
                                            <FormText className="item__price">{Math.floor(product.price * product.amount * 100) / 100} <span>BYN</span></FormText>
                                        </div>
                                    </FormGroup>
                                </Col>
                                <Col md={{size: 2, offset: 3}} sm={{size:12, offset: 0}} xs={{size: 5, offset: 3}}>
                                    <FormGroup className="basket__form-btns">
                                        <Button className="item__update" color="link">Обновить</Button>
                                        <a href="#!" onClick={this.toggleModalRemoveProduct} className="item__remove" color="link">Удалить</a>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Form>

                        <Modal isOpen={showModalRemoveProduct} toggle={this.toggleModalRemoveProduct}>
                            <ModalHeader toggle={this.toggleModalRemoveProduct}>Удалить товар из корзины?</ModalHeader>
                            <ModalFooter>
                                <Button color="danger" onClick={(e) => this.removeProduct(e, product.id)}>Удалить</Button>{' '}
                                <Button color="secondary" onClick={this.toggleModalRemoveProduct}>Отмена</Button>
                            </ModalFooter>
                        </Modal>

                    </Col>
                </Row>
            </Fragment>
        )
    }

    updateProduct = (e, id) => {
        e.preventDefault();
        let formData = new FormData(e.target);
        // console.log(formData.get("item__amount"))
        // Checking if < 0 || == 0
        if (+formData.get("item__amount") < 0) {
            this.setState({
                amountError: true
            });
            return;
        } else {
            this.setState({
                amountError: false
            });
        }
        if (+formData.get("item__amount") === 0) {
            this.setState({
                showModalRemoveProduct: !this.state.showModalRemoveProduct
            })
            return;
        }
        console.log(+formData.get("item__amount"))
        this.props.update(e, id, +formData.get("item__amount"))
    }

    removeProduct = (e, productId) => {
        this.setState({
            showModalRemoveProduct: !this.state.showModalRemoveProduct
        });
        this.props.remove(e, productId);
    }

    toggleModalRemoveProduct = () => {
        this.setState({
            showModalRemoveProduct: !this.state.showModalRemoveProduct
        })
    }
}
