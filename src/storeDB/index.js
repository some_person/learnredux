let storeDB = {
    auth: {
        isAuth: false
    },
    visibilityFilters: {
        activeFilters: {
            title: [],
            price: [],
        },
        filtersList: {
            title: [
                ["Ежедневники", "Ежедневник"],
                ["Часы", "Часы"],
                ["Рамки", "Рамка"],
                ["Фотоальбомы", "Фотоальбом"]
            ],
        }
    },
    items: [
        {
            id: 1, 
            title: "Ежедневник",
            src: "/assets/images/items/category01/01/main.jpg",
            gallery: [
                "/assets/images/items/category01/01/gallery/01.jpg",
                "/assets/images/items/category01/01/gallery/02.jpg",
                "/assets/images/items/category01/01/gallery/03.jpg",
            ],
            price: 57.4,
            status: "TOP",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis! Lorem ipsum dolor sit amet, consectetur adipisicing elit"
        },
        {
            id: 2,
            title: "Ежедневник",
            src: "/assets/images/items/category01/02/main.jpg",
            gallery: [
                "/assets/images/items/category01/02/gallery/01.jpg",
                "/assets/images/items/category01/02/gallery/02.jpg",
                "/assets/images/items/category01/02/gallery/03.jpg",
            ],
            price: 101.9,
            status: "TOP",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis."
        },
        {
            id: 3,
            title: "Ежедневник",
            src: "/assets/images/items/category01/03/main.jpg",
            gallery: [
                "/assets/images/items/category01/03/gallery/01.jpg",
                "/assets/images/items/category01/03/gallery/02.jpg",
                "/assets/images/items/category01/03/gallery/03.jpg",
                "/assets/images/items/category01/03/gallery/04.jpg",
            ],
            price: 30.1,
            status: "",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
        {
            id: 4,
            title: "Ежедневник",
            src: "/assets/images/items/category01/04/main.jpg",
            gallery: [
                "/assets/images/items/category01/04/gallery/01.jpg",
            ],
            price: 84.1,
            status: "",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis! Exercitationem fugiat, hic? Alias aliquam dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis."
        },
        {
            id: 5,
            title: "Часы",
            src: "/assets/images/items/category02/01/main.jpg",
            gallery: [
                "/assets/images/items/category02/01/gallery/01.jpg",
            ],
            price: 99.0,
            status: "TOP",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
        {
            id: 6,
            title: "Часы",
            src: "/assets/images/items/category02/02/main.jpg",
            gallery: [],
            price: 75.0,
            status: "",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. veritatis. Alias atque harum perferendis!"
        },
        {
            id: 7,
            title: "Часы",
            src: "/assets/images/items/category02/03/main.jpg",
            gallery: [],
            price: 8.2,
            status: "",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
        {
            id: 8,
            title: "Рамка",
            src: "/assets/images/items/category03/01/main.jpg",
            gallery: [
                "/assets/images/items/category03/01/gallery/01.jpg",
            ],
            price: 59.9,
            status: "TOP",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
        
        {
            id: 9,
            title: "Рамка",
            src: "/assets/images/items/category03/02/main.jpg",
            gallery: [
                "/assets/images/items/category03/02/gallery/01.jpg",
            ],
            price: 60.3,
            status: "TOP",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis.  Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis.Alias atque harum perferendis!"
        },
        
        {
            id: 10,
            title: "Рамка",
            src: "/assets/images/items/category03/03/main.jpg",
            gallery: [],
            price: 49.2,
            status: "",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
        
        {
            id: 11,
            title: "Рамка",
            src: "/assets/images/items/category03/04/main.jpg",
            gallery: [
                "/assets/images/items/category03/04/gallery/01.jpg",
            ],
            price: 46.5,
            status: "",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
        
        {
            id: 12,
            title: "Рамка",
            src: "/assets/images/items/category03/05/main.jpg",
            gallery: [
                "/assets/images/items/category03/05/gallery/01.jpg",
                "/assets/images/items/category03/05/gallery/02.jpg",
            ],
            price: 63.3,
            status: "",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
        {
            id: 13,
            title: "Фотоальбом",
            src: "/assets/images/items/category04/01/main.jpg",
            gallery: [
                "/assets/images/items/category04/01/gallery/01.jpg",
                "/assets/images/items/category04/01/gallery/02.jpg",
                "/assets/images/items/category04/01/gallery/03.jpg",
            ],
            price: 116.5,
            status: "TOP",
            full_text: "Lorem ipsum dolor sit amet, consectetur Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
        
        {
            id: 14,
            title: "Фотоальбом",
            src: "/assets/images/items/category04/02/main.jpg",
            gallery: [
                "/assets/images/items/category04/02/gallery/01.jpg",
            ],
            price: 65.0,
            status: "",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
        
        {
            id: 15,
            title: "Фотоальбом",
            src: "/assets/images/items/category04/03/main.jpg",
            gallery: [
                "/assets/images/items/category04/03/gallery/01.jpg",
                "/assets/images/items/category04/03/gallery/02.jpg",
            ],
            price: 6.3,
            status: "TOP",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
        
        {
            id: 16,
            title: "Фотоальбом",
            src: "/assets/images/items/category04/04/main.jpg",
            gallery: [
                "/assets/images/items/category04/04/gallery/01.jpg",
                "/assets/images/items/category04/04/gallery/02.jpg",
                "/assets/images/items/category04/04/gallery/03.jpg",
            ],
            price: 91.6,
            status: "TOP",
            full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem fugiat, hic? Alias aliquam at culpa cupiditate deserunt dolores ducimus, earum facere mollitia reprehenderit suscipit temporibus, veritatis. Alias atque harum perferendis!"
        },
    ],
    basket: [
        {
            id: 1,
            amount: 1,
        },
        {
            id: 2,
            amount: 1
        },
        {
            id: 3,
            amount: 1
        },
    ]
};

export default storeDB;