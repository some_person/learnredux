import storeDB from "./../storeDB/index";

let initialState = storeDB.items;

export default function Products (state = [...initialState], action) {

    switch (action.type) {
        case ("PASS_SORTED_PRODUCTS_BY_PRICE") : {
            return [...state];
        }

        default : {
            return [...state];
        }
    }
}