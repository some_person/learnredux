import storeDB from "./../storeDB/index";

let initialState = storeDB.visibilityFilters;
export default function Filters (state = initialState, action) {
    switch (action.type) {
        case ("SET_FILTER_BY_TITLE") : {
            let titles = [...state.activeFilters.title];
            if(state.activeFilters.title.some(item => item === action.data)) {
                titles.splice(titles.indexOf(action.data), 1);
                return  Object.assign({}, state, {activeFilters: {...state.activeFilters, title: titles}});
            } else {
                return Object.assign({}, state, {activeFilters: {...state.activeFilters, title: [...state.activeFilters.title, action.data]}});
            }
        }

        case ("CLEAN_FILTER_BY_TITLE") : {
            return Object.assign({}, state, {activeFilters: {...state.activeFilters, title: []}});
        }

        case ("SET_FILTER_BY_PRICE") : {
            return Object.assign({}, state, {activeFilters: {...state.activeFilters, price: [...action.data]}});
        }

        case ("CLEAN_FILTER") : {
            let filters = {};
            for (let filterKey in state.activeFilters) {
                if (state.activeFilters.hasOwnProperty(filterKey)) {
                    filters = {...filters, [filterKey]: []}
                }
            }
            return Object.assign({}, state, {activeFilters: {...filters}});
        }

        default : {
            return state;
        }
    }
}