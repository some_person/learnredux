import storeDB from "./../storeDB/index";

let initialState = storeDB.auth;
export default function Auth (state = {...initialState}, action) {

    switch (action.type) {

        case ("SET_AUTH") : {
            let matches = document.cookie.match(new RegExp(
                "(?:^|; )" + action.data.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? Object.assign({}, {...state, isAuth: true}) : Object.assign({}, {...state, isAuth: false});
        }

        case ("LOGIN") : {
            let options = {
                path: '/',
                ...action.data.options
            };

            if (options.expires instanceof Date) {
                options.expires = options.expires.toUTCString();
            }

            let updatedCookie = encodeURIComponent(action.data.name) + "=" + encodeURIComponent(action.data.value);

            for (let optionKey in options) {
                updatedCookie += "; " + optionKey;
                let optionValue = options[optionKey];
                if (optionValue !== true) {
                    updatedCookie += "=" + optionValue;
                }
            }

            document.cookie = updatedCookie;

            return Object.assign({}, {...state, isAuth: true});
        }

        case ("LOGOUT") : {
            document.cookie = `${action.data}= ; max-age=-1`;

            return Object.assign({}, {...state, isAuth: false});
        }

        default : {
            return Object.assign({}, state);
        }
    }
}