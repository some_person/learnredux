import storeDB from "./../storeDB/index";
import productGalleryMini from "../components/sliders/productGalleryMini";

let initialState = storeDB.basket;
export default function Filters (state = initialState, action) {
    switch (action.type) {

        case ("ADD_TO_BASKET") : {
            if (state.some(item => item.id === action.data.id)) {
                return [...state].map(item => {
                    if (item.id === action.data.id) {
                        console.log(item.id)
                        let amount = item.amount + 1;
                        return {
                            ...item,
                            amount: amount,
                        }
                    }
                    return item;
                })
            } else {
                return [...state, {id: action.data.id, amount: 1}]
            }
        }

        case ("UPDATE_BASKET_PRODUCT_AMOUNT") : {
            return [...state].map(item => {
                if (item.id === action.data.id) {
                    // console.log(item.id)
                    return {
                        ...item,
                        amount: action.data.amount,
                    }
                }
                return item;
            })
        }

        case ("REMOVE_PRODUCT") : {
            return [...state].filter(item => item.id !== action.data.id);
        }

        case ("CLEAR_BASKET") : {
            let _state = [...state];
            _state.length = 0;
            return _state;
        }

        default : {
            return state;
        }
    }
}