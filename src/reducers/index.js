import {combineReducers} from "redux";
import Filters from "./Filters";
import Products from "./Products";
import Basket from "./Basket";
import Auth from "./Auth";

export default combineReducers({
    filters: Filters,
    products: Products,
    basket: Basket,
    auth: Auth,
})