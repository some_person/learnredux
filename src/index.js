import './scss/style.scss';
import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';

import {createStore} from "redux";
import {Provider} from "react-redux";
import Reducer from "./reducers/index"
import App from './components/app';
const store = createStore(Reducer);
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("app")
);


