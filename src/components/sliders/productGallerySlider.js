import Carousel, { consts } from 'react-elastic-carousel';
import React , {Fragment, Component} from "react";

export default class ProductGallerySlider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mainImgSrc: "",
        };
        this.breakPoints = [
            { width: 576, itemsToShow: 2, itemsToScroll: 2 },
        ]
    }

    render() {
        let {gallery} = this.props;
        let j = 0;
        return (
            <Fragment>
                <div className="product-gallery">
                    <div className="product-gallery__item">
                        <img src={this.state.mainImgSrc} alt={`image#`}/>
                    </div>
                </div>
                <Carousel breakPoints={this.breakPoints} className="product-carousel" itemsToShow={3} renderArrow={this.carouselArrows}>
                    {gallery.map(src => {
                    j++;
                    return <div onClick={(e) => this.toggleSlide(e, src)} className="product-carousel__item" key={j}>
                        <img src={src} alt={`image#${j}`}/>
                    </div>
                    })}
                </Carousel>
            </Fragment>
        )
    }

    carouselArrows({ type, onClick, isEdge }) {
        const pointer = type === consts.PREV ? <div className={`product-carousel__arrow product-carousel__arrow-prev`}><span></span></div> : <div className={`product-carousel__arrow product-carousel__arrow-next`}><span></span></div>
        return (
            <div onClick={onClick} disabled={isEdge} className={isEdge ? "arrow-dis" : ""}>
                {pointer}
            </div>
        )
    }

    toggleSlide = (e, src) => {
        this.setState({
            mainImgSrc: src
        })

    }

    componentDidMount() {
        this.setState({
            mainImgSrc: this.props.gallery[0]
        })

    }
}