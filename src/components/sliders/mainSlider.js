import React , {Fragment, Component} from "react";
import {connect} from "react-redux";
import Carousel, { consts } from 'react-elastic-carousel';
import CarouselItem from "./../../pages/catalog/item";

class MainSlider extends Component {
    constructor(props) {
        super(props);

        this.breakPoints = [
            { width: 1, itemsToShow: 1 },
            { width: 350, itemsToShow: 2, itemsToScroll: 1 },
            { width: 550, itemsToShow: 3, itemsToScroll: 2 },
            { width: 870, itemsToShow: 3 },
            { width: 1150, itemsToShow: 4, itemsToScroll: 2 },
            { width: 1450, itemsToShow: 5 },
            { width: 1750, itemsToShow: 6 },
        ]
    }

    render() {
        let {products} = this.props;
        let topProducts = products.filter(product => product.status === "TOP");
        let i = 0;
        return(
            <Fragment>
                <Carousel className="product-carousel main-carousel" itemPadding={[0, 0]} itemsToShow={2} breakPoints={this.breakPoints} renderArrow={this.carouselArrows}>
                    {topProducts.map(product => {
                        i++;
                        return <div className="product-carousel__item main-carousel__item" key={product.id}>
                            <CarouselItem product={product} />
                        </div>
                    })}
                </Carousel>
            </Fragment>
        )
    }



    carouselArrows({ type, onClick, isEdge }) {
        const pointer = type === consts.PREV ? <div className={`product-carousel__arrow product-carousel__arrow-prev`}><span></span></div> : <div className={`product-carousel__arrow product-carousel__arrow-next`}><span></span></div>
        return (
            <div onClick={onClick} disabled={isEdge} className={isEdge ? "arrow-dis" : ""}>
                {pointer}
            </div>
        )
    }
}

let mapStateFromProps = (store) => {
    return {products: store.products}
}
export default connect(mapStateFromProps)(MainSlider)