import Slider from "react-slick";
import React , {Fragment, Component} from "react";
import {connect} from "react-redux";
import { FaChevronLeft } from 'react-icons/fa';
import { FaChevronRight } from 'react-icons/fa';
import ProductGallerySlider from "./../../components/sliders/productGallerySlider";

export default class productGalleryMini extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        let {gallery} = this.props;
        console.log(gallery);
        const settings = {
            dots: false,
            autoplay: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 2,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />,
            asNavFor: <ProductGallerySlider/>
        };
        let i = 0;
        return (
            <Slider {...settings}>
                {gallery.map(src => {
                    i++;
                    return <div className="slider__item slider-mini__item" key={i}>
                        <img src={src} alt={`image#${i}`}/>
                    </div>
                })}
            </Slider>
        );
    }
}

function SampleNextArrow(props) {
    const { onClick, className } = props;
    return (<FaChevronRight className={className} onClick={onClick} />
    );
}

function SamplePrevArrow(props) {
    const { onClick, className} = props;
    return (<FaChevronLeft className={className} onClick={onClick}/>
    );
}

// let mapStateFromProps = (store) => {
//     return {products: store.products}
// }
// export default connect(mapStateFromProps)(ProductGallerySlider)