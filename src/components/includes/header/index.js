import "./scss/style.scss";
import React , {Fragment, Component} from "react";
import {Card, CardBody, CardTitle, CardSubtitle, Badge, CardImg,
    Container, Row, Col} from "reactstrap";
import {Link} from "react-router-dom";
import {connect} from "react-redux";

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuActive: false
        }
    }

    render() {
        let {auth, basket} = this.props.store;
        let generalAmount = basket.reduce((previousVal, currentVal) => {
            return previousVal + (+currentVal.amount)
        }, 0);
        return (
            <Fragment>
                <header className="header">
                    <Container>
                        <Row className="header__body">
                            <Col lg="4" md="4" sm="4" xs="5">
                                <Link to="/" className="header__logo">
                                    <img src="./../assets/images/logo/logo.jpg" alt="logo"/>
                                </Link>
                            </Col>
                            <Col lg={{size:6, offset: 2}} md={{size:7, offset: 1}} sm={{size:12}} className={this.state.menuActive ? 'header__menu active' : 'header__menu'}>
                                <ul className="header__list">
                                    <li>
                                        <Link onClick={this.toggleMenu} to="/" className="header__link">На главную</Link>
                                    </li>
                                    <li>
                                        <Link onClick={this.toggleMenu} to="/catalog" className="header__link">Каталог</Link>
                                    </li>
                                    <li>
                                        <Link onClick={this.toggleMenu} to="/basket" className="header__link">Корзина ({generalAmount})</Link>
                                    </li>
                                    {auth.isAuth ?
                                        <li>
                                            <a href="#!" onClick={this.logout} className="header__link">Выйти</a>
                                        </li> :
                                        <li>
                                            <Link onClick={this.toggleMenu} to="/login" className="header__link">Войти</Link>
                                        </li>
                                    }
                                </ul>
                            </Col>
                            <div className={`header__burger ${this.state.menuActive ? "active" : ""}`} onClick={this.toggleMenu}>
                                <span></span>
                            </div>
                        </Row>
                    </Container>
                </header>
            </Fragment>
        )
    }

    toggleMenu = () => {
        if (window.innerWidth <= 768) {
            this.setState({
                menuActive: !this.state.menuActive
            })
            document.body.className = !this.state.menuActive ? "lock" : ""
        }
    }

    logout = (e) => {
        e.preventDefault();
        this.toggleMenu();
        this.props.dispatch({
            type: "LOGOUT",
            data: "isAuth",
        })
    }
}


let mapStateFromProps = (store) => {
    return {store: store}
}
export default connect(mapStateFromProps)(Header)