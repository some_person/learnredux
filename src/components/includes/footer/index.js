import "./scss/style.scss";
import React , {Fragment, Component} from "react";
import {Container, Row, Col} from "reactstrap";


export default class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Fragment>
                <footer className="footer">
                    <Container>
                        <p className="footer__text">Copyright © 2020 Some Person</p>
                    </Container>
                </footer>
            </Fragment>
        )
    }
}