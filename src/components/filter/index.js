import React , {Fragment, Component} from "react";
import { ListGroup, ListGroupItem,
    Container, Row, Col,
    Button, Form, FormGroup, Label, Input, FormText} from "reactstrap";
import {connect} from "react-redux";


class Filter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            minPrice: 6,
            maxPrice: 117,
            spoilerPriceActive: false,
            spoilerTitleActive: false,
            showFilterTitleMobile: false,
            showFilterPriceMobile: false,
            sort: "desc"
        }
    }

    render() {
        let {filters,filtersList} = this.props;
        let {sort} = this.state;
        let {showFilterTitleMobile, showFilterPriceMobile, spoilerTitleActive, spoilerPriceActive} = this.state;
        let i = 0;

        return(
            <Fragment>
                <Row className="catalog__filter filter">
                    <Col md={12} className={`filter__spoiler filter__spoiler_title ${spoilerTitleActive ? "spoiler-active" : ""}`} onClick={(e) => this.toggleMobileFilter(e, "spoilerTitleActive", "showFilterTitleMobile")}>
                        <p>Категории</p>
                    </Col>
                    <Col lg={6} md={12} className="filter__filter-by-title">
                        <ul className={`filter__list ${showFilterTitleMobile ? "active" : ""}`}>
                            <li>
                                <a href="#!" onClick={(e) => this.setFilterByTitle(e, "CLEAN_FILTER_BY_TITLE")} className="filter__link">Все</a>
                            </li>
                            {filtersList.title.map(filter => {
                                i++;
                                let isActive = filters.title.some(item => item === filter[1]);
                                console.log(isActive)
                                return <li key={i}>
                                    <a href="#!" className={`filter__link ${isActive ? "active" : ""}`} onClick={(e) => this.setFilterByTitle(e, filter[1])}>{filter[0]}</a>
                                </li>
                            })}
                        </ul>
                    </Col>
                    <Col md={12} className={`filter__spoiler filter__spoiler_price ${spoilerPriceActive ? "spoiler-active" : ""}`} onClick={(e) => this.toggleMobileFilter(e, "spoilerPriceActive", "showFilterPriceMobile")}>
                        <p>Цена</p>
                    </Col>
                    <Col lg={{size: 5, offset: 1}} md={12} className="filter__filter-by-price">
                        <Form onSubmit={(e) => this.setFilterByPrice(e)} className={`filter__form form-filter ${showFilterPriceMobile ? "active" : ""}`}>
                            <Row form className="form-filter__row">
                                <Col lg={2} md={0}>
                                    <p>Цена:</p>
                                </Col>
                                <Col lg={3} md={4} sm={6} xs={6}>
                                    <FormGroup className="form-filter__input-group">
                                        <Label for="filterPriceFrom">От</Label>
                                        <Input type="number" name="from" id="filterPriceFrom" defaultValue={this.state.minPrice} required />
                                    </FormGroup>
                                </Col>
                                <Col lg={3} md={4} sm={6} xs={6}>
                                    <FormGroup className="form-filter__input-group">
                                        <Label for="filterPriceTo">До</Label>
                                        <Input type="number" name="to" id="filterPriceTo" defaultValue={this.state.maxPrice} required />
                                    </FormGroup>
                                </Col>
                                <Col lg={4} md={{size:4, offset: 0}} sm={{size: 4, offset: 8}} xs={{size: 12, offset: 0}}>
                                    <Button className="form-filter__submit">Применить</Button>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
                <Row>
                    <Col lg={4} md={4} sm={5} className="filter__clean">
                        <a href="#!" onClick={this.cleanFilter}>Очистить фильтр</a>
                    </Col>
                    <Col lg={{size:4, offset: 4}} md={{size:5, offset: 3}} sm={{size:7, offset: 0}} className="filter__sort">
                        <Button color="info" onClick={(e) => this.toggleSort(e, "price")}>Сортировать по цене <span className="sortBtn">({sort === "desc" ? "убыв." : "возр."})</span>
                        </Button>
                    </Col>
                </Row>
            </Fragment>
        );
    }

    setFilterByTitle = (e, title) => {
        e.preventDefault();
        this.props.dispatch({
            type: title === "CLEAN_FILTER_BY_TITLE" ? "CLEAN_FILTER_BY_TITLE" : "SET_FILTER_BY_TITLE",
            data: title
        })
    }

    setFilterByPrice = (e) => {
        e.preventDefault();
        let from = e.target.querySelector("#filterPriceFrom");
        let to = e.target.querySelector("#filterPriceTo")
        this.checkMinPrice(from, to);
        this.checkMaxPrice(from, to);
        let formData = new FormData(e.target);
        this.props.dispatch({
            type: "SET_FILTER_BY_PRICE",
            data: [formData.get("from"), formData.get("to")]
        })
    }

    checkMinPrice = (min, max) => {
        if(+min.value < this.state.minPrice || +min.value > this.state.maxPrice || +min.value > max.value) {
            min.value = this.state.minPrice
        }
    }

    checkMaxPrice = (min, max) => {
        if(+max.value > this.state.maxPrice || +max.value < this.state.minPrice || +max.value < min.value) {
            max.value = this.state.maxPrice
        }
    }


    toggleMobileFilter = (e, spoiler, filter) => {
        console.log(spoiler, filter)
        this.setState({
            [spoiler]: !this.state[spoiler],
            [filter]: !this.state[filter],
        })
    }

    cleanFilter = (e) => {
        e.preventDefault();
        let from = document.querySelector("#filterPriceFrom");
        let to = document.querySelector("#filterPriceTo");
        from.value = this.state.minPrice;
        to.value = this.state.maxPrice;
        this.props.dispatch({
            type: "CLEAN_FILTER",
            data: ""
        })
    }

    toggleSort = (e) => {
        this.setState({
            sort: this.state.sort === "desc" ? "asc" : "desc",
        });
        this.sortProductsByPrice()
    }

    sortProductsByPrice = () => {
        let itemsForSort = this.props.products;
        itemsForSort.sort((a, b) => {
            if (this.state.sort === "asc") {
                return a.price - b.price
            } else if (this.state.sort === "desc") {
                return b.price - a.price
            }
        });
        this.props.dispatch({
            type: "PASS_SORTED_PRODUCTS_BY_PRICE",
            data: [...itemsForSort]
        })
    }
}

let mapStateFromProps = (store) => {
    return {filters: store.filters.activeFilters,
        filtersList:store.filters.filtersList,
        products: store.products
    }
}
export default connect(mapStateFromProps)(Filter)