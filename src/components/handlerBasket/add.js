import React , {Fragment, Component} from "react";
import {connect} from "react-redux";
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button,
    Row, Col, Container
} from 'reactstrap';

class Add extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showModalAddedToBasket: false
        }
    }

    render() {
        let {productId} = this.props;
        return ( <Fragment>
                <Button  className="add-to-basket-btn" onClick={(e) => this.add(e, productId)}>В корзину</Button>
            </Fragment>
        )
    }

    add = (e, productId) => {
        e.preventDefault();
        this.props.dispatch({
            type: "ADD_TO_BASKET",
            data: {
                id: productId
            }
        })
        this.props.toggleModalAddedToBasket();
    }

    toggleModalAddedToBasket = () => {
        this.setState({
            showModalRemoveProduct: !this.state.showModalAddedToBasket
        })
    }
}

let mapStateFromProps = (store) => {
    return {store: store.basket}
}

export default connect(mapStateFromProps)(Add)