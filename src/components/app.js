import React , {Fragment, Component} from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {connect} from "react-redux";

import Header from "./includes/header";
import Footer from "./includes/footer";
import Main from "./../pages/index";
import Catalog from "./../pages/catalog";
import Product from "./../pages/product";
import Basket from "./../pages/basket";
import Login from "./../pages/login";
import Page404 from "./../pages/404";

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {auth} = this.props.store
        return(
            <Fragment>
                <Router>
                    <Header/>
                        <main className="main">
                            <Switch>
                                <Route exact path="/" component={Main}/>

                                <Route exact path="/catalog" component={Catalog}/>

                                <Route exact path="/basket" component={auth.isAuth ? Basket : Login}/>

                                <Route exact path="/product/:id" component={Product}/>

                                <Route exact path="/login" component={auth.isAuth ? Main : Login}/>

                                <Route component={Page404}/>
                            </Switch>
                        </main>
                    <Footer/>
                </Router>
            </Fragment>
        );
    }

    componentDidMount() {
        this.props.dispatch({
            type: "SET_AUTH",
            data: "isAuth"
        })
    }
}


let mapStateFromProps = (store) => {
    return {store: store}
}
export default connect(mapStateFromProps)(App)